import * as types from '../constants';

const initialState = {
  data: [],
  isLoading: false,
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_INITIAL_DATA_STARTED:
      return Object.assign({}, state, { isLoading: true });
    case types.REQUEST_INITIAL_DATA_SUCCESSFULL:
      return Object.assign({}, state, { data: action.data, isLoading: false });
    case types.REQUEST_INITIAL_DATA_FAILED:
      return Object.assign({}, state, { error: action.error, isLoading: false });
    default:
      return state;
  }
}
