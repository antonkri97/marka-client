import React from 'react';
import Doctor from './Doctor';
import { Col } from 'react-bootstrap';
import Spinner from 'react-loading-spinner';
import PropTypes from 'prop-types';

const Doctors = ({ doctors, isLoading }) => (
  <Spinner isLoading={isLoading} spinner={() => <h3>Загрузка</h3>}>
    {
      doctors && <div>
        <h3>Наши специалисты!</h3>
        {
          doctors.map((d, k) => (
            <Col xs={6} key={k}>
              <Doctor name={d.doctorName} photo={d.photo} specialityName={d.specialityName} />
            </Col>
          ))
        }
      </div>
    }
  </Spinner>
);

Doctors.propTypes = {
  doctors: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default Doctors;
