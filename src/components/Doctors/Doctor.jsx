import React from 'react';
import PropTypes from 'prop-types';
import { Image, Panel, Col } from 'react-bootstrap';

const Doctor = ({ name, photo, specialityName }) => (
  <Panel>
    <Col xs={12} md={6}>
      <Image src={photo} thumbnail />
    </Col>
    <Col xs={12} md={6}>
      <h4>{name}</h4>
      <h5>{specialityName}</h5>
    </Col>
  </Panel>
);

Doctor.propTypes = {
  name: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
  specialityName: PropTypes.string.isRequired
};

export default Doctor;
