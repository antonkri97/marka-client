import React from 'react';
import Review from './Review';
import PropTypes from 'prop-types';
import { Col } from 'react-bootstrap';
import Spinner from 'react-loading-spinner';

const Reviews = ({ reviews, isLoading }) => {
  return (
    <Spinner isLoading={isLoading} spinner={() => <h3>Загрузка отзывов</h3>}>
      {
        reviews && <div>
          {
            reviews.map((r, k) => (
              <Col xs={12} key={k}>
                <Review client={r.clientName} doctorName={r.doctorName} photo={r.photo} body={r.body} speciality={r.specialityName}/>
              </Col>
            ))
          }
        </div>
      }
    </Spinner>
  )
};

Reviews.propTypes = {
  reviews: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default Reviews;
