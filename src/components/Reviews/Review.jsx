import React from 'react';
import PropTypes from 'prop-types';
import { Thumbnail } from 'react-bootstrap';

const Review = ({ client, body, doctorName, photo, speciality }) => (
  <Thumbnail src={photo}>
    <h3>{doctorName} - {speciality}</h3>
    <blockquote>
      <p>{body}</p>
      <footer>{client}</footer>
    </blockquote>
  </Thumbnail>
);

Review.propTypes = {
  client: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  doctorName: PropTypes.string.isRequired,
  speciality: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired
};

export default Review;
