import React from 'react';
import News from './News';
import PropTypes from 'prop-types';
import { PanelGroup, Col } from 'react-bootstrap';
import Spinner from 'react-loading-spinner'

const NewsGroup = ({ news, isLoading }) => (
  <Col xs={12} md={6}>
    <Spinner isLoading={isLoading} spinner={() => <h2>Загрузка новостей</h2>}>
    {
      news && <div>
        <h3>Последние новости клиники</h3>
        <PanelGroup>
          {
            news.map((n, k) => <News key={k} title={n.title} body={n.body} date={n.date}/>)
          }
        </PanelGroup>
      </div>
    }
    </Spinner>
  </Col>
);

NewsGroup.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  news: PropTypes.array.isRequired
};

export default NewsGroup;
