import React from 'react';
import PropTypes from 'prop-types';
import { Panel } from 'react-bootstrap';

const News = ({ title, body, date, key }) => (
  <Panel header={title} footer={date}>
    {body}
  </Panel>
);

News.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};

export default News;
