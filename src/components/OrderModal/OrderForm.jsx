import React from 'react';
import FieldGroup from './FieldGroup';
import TelField from './TelField';
import DateField from './DateField';
import SubmitField from './SubmitField';

const OrderForm = () => (
  <div>
    <FieldGroup id='nameField' type='text' label='Имя' />
    <TelField />
    <DateField />
    <FieldGroup id='timeField' type='time' label='Удобное время для посещения' />
    <SubmitField />
  </div>
);

export default OrderForm;
