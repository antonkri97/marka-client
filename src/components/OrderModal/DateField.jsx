import React, { Component }             from 'react';
import PropTypes                        from 'prop-types';
import DatePicker                       from 'react-bootstrap-date-picker';
import { FormGroup, ControlLabel, Col } from 'react-bootstrap';

class DateField extends Component {
  constructor(args) {
    super(args);

    const date = '1999-01-01T03:53:18.344Z';

    this.state = {
      value: date
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value, formattedValue) {
    this.setState({
      value,
      formattedValue
    });
  }

  render() {
    return (
      <Col sm={this.props.cols}>
        <FormGroup>
          <ControlLabel>Дата рождения</ControlLabel>
          <DatePicker id='example-datepicker' value={this.state.value} onChange={this.handleChange} />
        </FormGroup>
      </Col>
    );
  }
}

DateField.propTypes = {
  cols: PropTypes.number
};

export default DateField;
