import React                from 'react';
import { Button, Col, Row } from 'react-bootstrap';

const style = {
  marginTop: '2%'
};

const SubmitField = () => (
  <Row>
    <Col sm={2} style={style} >
      <Button>
        Оставить заявку
      </Button>
    </Col>
  </Row>
);

export default SubmitField;