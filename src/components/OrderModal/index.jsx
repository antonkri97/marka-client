import React, { Component } from 'react';
import { Modal, ModalTitle, ModalBody, ModalHeader } from 'react-bootstrap'
import PropTypes from 'prop-types';
import OrderForm from './OrderForm';

export default class OrderModal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired
  }

  render() {
    return (
      <Modal show={this.props.isOpen} onHide={this.props.onClose}>
        <ModalHeader closeButton>
          <ModalTitle>Оставьте заявку и мы вам перезвоним!</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <OrderForm />
        </ModalBody>
      </Modal>
    );
  }
}
