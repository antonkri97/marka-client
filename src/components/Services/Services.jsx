import React from 'react';
import Service from './Service';
import PropTypes from 'prop-types';
import Spinner from 'react-loading-spinner';
import { Col } from 'react-bootstrap'

const Services = ({ services, isLoading }) => (
  <Spinner isLoading={isLoading} spinner={() => <h3>Загрузка доступных услуг</h3>}>
    {
      services && <div>
        {
          services.map((s, k) => <Col xs={12} md={4} key={k}>
              <Service name={s.name} photo={s.photo} price={s.price} />
            </Col>
          )
        }
      </div>
    }
  </Spinner>
)

Services.propTypes = {
  services: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default Services;
