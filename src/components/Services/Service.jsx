import React, { Component }                   from 'react';
import { Thumbnail, Button } from 'react-bootstrap';
import PropTypes                  from 'prop-types';
import OrderModal from '../OrderModal';

export default class Service extends Component {
  state = { isOpenOrderModal: false }

  static propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    photo: PropTypes.string.isRequired
  }

  openModal = () => this.setState({ isOpenOrderModal: true })

  closeModal = () => this.setState({ isOpenOrderModal: false })

  render() {
    const {
      name, price, photo
    } = this.props;
    return (
      <Thumbnail src={photo} alt={name}>
        <h3>{name}</h3>
        <p>{price} &#8381;</p>
        <p>
          <Button onClick={this.openModal}>
            Записаться на приём
            <OrderModal isOpen={this.state.isOpenOrderModal} onClose={this.closeModal}/>
          </Button>
        </p>
      </Thumbnail>
    )
  }
}
