import React                           from 'react';
import PropTypes                       from 'prop-types';
import { Grid, Nav, Navbar, NavItem }  from 'react-bootstrap';
import LinkContainer                  from 'react-router-bootstrap/lib/LinkContainer';
import { Link } from 'react-router';

import '../../style/bootstrap.css';

const App = ({ children }) => (
  <div>
    <Navbar>
      <Navbar.Header>
        <Link to='/'>
          <Navbar.Brand>
            Марка
          </Navbar.Brand>
        </Link>
        <Navbar.Toggle/>
      </Navbar.Header>
        <Navbar.Collapse>
          <Nav navbar>
            <LinkContainer to='/main'>
              <NavItem>Главная</NavItem>
            </LinkContainer>
            <LinkContainer to='/doctors'>
              <NavItem>Наши врачи</NavItem>
            </LinkContainer>
            <LinkContainer to='/services'>
              <NavItem>Услуги</NavItem>
            </LinkContainer>
            <LinkContainer to='/review'>
              <NavItem>Отзывы</NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    <Grid>
      {children}
    </Grid>
  </div>
);

App.propTypes = {
  children: PropTypes.node.isRequired
};

export default App;
