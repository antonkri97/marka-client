import React from 'react';
import ReactDOM from 'react-dom';
import routes from './routes';
import { browserHistory, Router } from 'react-router'
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from './reducers';
import requestInitialData from './actions/api';

const store = createStore(reducers, applyMiddleware(thunk, logger));

store.dispatch(requestInitialData());

const component = (
  <Provider store={store}>
    <Router history={browserHistory}>
      {routes}
    </Router>
  </Provider>
);

ReactDOM.render(component, document.getElementById('root'));
registerServiceWorker();
