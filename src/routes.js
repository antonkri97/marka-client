import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App/App';
import MainPage from './containers/MainPage';
import DoctorsPage from './containers/DoctorsPage';
import ServicesPage from './containers/ServicesPage';
import ReviewsPage from './containers/ReviewsPage';

export default (
  <Route component={App} path='/'>
    <IndexRoute component={MainPage} />
    <Route component={MainPage} path='/main' />
    <Route component={DoctorsPage} path='/doctors' />
    <Route component={ServicesPage} path='/services' />
    <Route component={ReviewsPage} path='/review' />
  </Route>
);
