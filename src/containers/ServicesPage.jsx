import { connect } from 'react-redux';
import Services from '../components/Services/Services';

const mapStateToProps = state => ({
  services: state.api.data.length === 0 ? [] : state.api.data.services,
  isLoading: state.api.isLoading
});

export default connect(mapStateToProps)(Services);
