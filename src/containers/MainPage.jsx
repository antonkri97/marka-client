import { connect } from 'react-redux';
import NewsGroup from '../components/News/NewsGroup';
import About from '../components/About';
import React from 'react';

const MainPage = ({ isLoading, news }) => (
  <div>
    <About />
    <NewsGroup isLoading={isLoading} news={news} />
  </div>
)

const mapStateToProps = (state) => ({
  isLoading: state.api.isLoading,
  news: state.api.data.length !== 0 ? state.api.data.news : []
});

export default connect(mapStateToProps)(MainPage);
