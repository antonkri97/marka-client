import { connect } from 'react-redux';
import Reviews from '../components/Reviews/Reviews';

const mapStateToProps = state => {
  if (state.api.data.length === 0) {
    return {
      reviews: [],
      isLoading: true
    };
  }

  const { doctors, reviews, clients, specialities } = state.api.data;

  let mergedReview = reviews.map(r => Object.assign({}, r,
                        doctors.find(d => d.id === r.doctorId),
                        clients.find(c => c.id === r.clientId),
                       ));

  mergedReview = mergedReview.map(r => Object.assign({}, r, specialities.find(s => s.id === r.specialityId)));

  console.log(mergedReview);

  return {
    reviews: mergedReview,
    isLoading: state.api.isLoading
  };
};

export default connect(mapStateToProps)(Reviews);
