import { connect } from 'react-redux';
import Doctors from '../components/Doctors/Doctors';

const mapStateToProps = (state) => {
  if (state.api.data.length === 0) {
    return {
      doctors: [],
      isLoading: true
    };
  }

  const { doctors, specialities } = state.api.data;

  const merjedDoctor = doctors.map(d => Object.assign({}, d, specialities.find(s => s.id === d.specialityId)));

  console.log(merjedDoctor)

  return {
    doctors: merjedDoctor,
    isLoading: state.api.isLoading
  };
};

export default connect(mapStateToProps)(Doctors);
