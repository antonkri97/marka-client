import * as types from '../constants';

const start = () => ({ type: types.REQUEST_INITIAL_DATA_STARTED });

const ok = (data) => ({ type: types.REQUEST_INITIAL_DATA_SUCCESSFULL, data });

const error = (error) => ({ type: types.REQUEST_INITIAL_DATA_FAILED, error });

export default () => async (dispatch) => {
  dispatch(start());

  try {
    const req = await fetch('http://localhost:8080/db');

    if (req.status !== 200) {
      dispatch(error(req.statusText));
    }

    const data = await req.json();

    setTimeout(() => {
      dispatch(ok(data))
    }, 1000)
  } catch (e) {
    dispatch(error(e.message))
  }
}